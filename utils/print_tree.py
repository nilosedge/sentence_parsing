
def print_tree(node, node_list, edge_list, level = 0):
	#print('\t' * level, node.orth_)
	
	if node.n_lefts + node.n_rights > 0:
		for child in node.children:
			if child.is_punct:
				#print("Punct: ", child)
				if len(list(child.children)) == 0:
					continue
			if child.is_stop:
				#print("Stop word: ", child)
				if len(list(child.children)) == 0:
					continue
			node_list[node.i][1] = False
			node_list[child.i][1] = False
			edge_list.append([node, child])
			print_tree(child, node_list, edge_list, level + 1)
